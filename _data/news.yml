- date: 15 October 2024
  headline: "New paper in [Physical Review Letters](https://link.aps.org/doi/10.1103/PhysRevLett.133.166703) by Robbie Elbertse _et al._, featured in [APS Physics](https://link.aps.org/doi/10.1103/Physics.17.s118), demonstrating a tremendous enhancement of the magnetic lifetime of a spin chain when tuned in the vicinity of a diabolic point. [<img src='/images/newspic/diabolic.png' class='img-responsive' style='width: 100%; max-width: 200px' />](https://link.aps.org/doi/10.1103/PhysRevLett.133.166703)"

- date: 28 September 2024
  headline: "In a collaboration with the [Center for Quantum Nanoscience](https://qns.science/), Rik Broekhoven published a paper in [npj Quantum Information](https://doi.org/10.1038/s41534-024-00888-9), describing a method for generating and certifying entanglement between magnetic atoms on a surface."

- date: 11 September 2024
  headline: "In [Nature Communications](https://doi.org/10.1038/s41467-024-52270-0), Lukas Veldman and coworkers demonstrate the possibility to interact coherently with the nucleus of a single atom. See our [press release](https://www.tudelft.nl/en/2024/tnw/quantum-researchers-cause-controlled-wobble-in-the-nucleus-of-a-single-atom) for more details."

- date: 27 May 2024
  headline: "Today Lukas Veldman defended his [PhD thesis](https://doi.org/10.4233/uuid:c3e888a9-cfaa-4d67-8ff2-74deebb6ee46) _Coherent dynamics of atomic spins on a surface_, for which he was awarded the prestigious _cum laude_ degree. Congratulations dr. Veldman! [<img src='/images/newspic/PhD_Lukas_zoom.jpg' class='img-responsive' style='width: 100%; max-width: 160px' />](https://doi.org/10.4233/uuid:c3e888a9-cfaa-4d67-8ff2-74deebb6ee46)"

- date: 13 July 2023
  headline: "Have a look at this beautiful short [promotion video](https://www.youtube.com/watch?v=NE2tugHqxN4) that explains exactly what we do in our group."

- date: 6 July 2023
  headline: "Sander Otte was awarded an ERC Advanced Grant on his proposal _HYPSTER_. More information in the [press release](https://www.tudelft.nl/en/2023/tnw/erc-advanced-grant-voor-sander-otte). [<img src='/images/logopic/HYPSTER_logo.png' class='img-responsive' style='width: 100%; max-width: 200px' />](https://www.tudelft.nl/en/2023/tnw/erc-advanced-grant-voor-sander-otte)"

- date: 12 January 2023
  headline: "Check out our paper in [Phys. Rev. B](https://doi.org/10.1103/PhysRevB.107.035406) by Rasa Rejali _et al._, presenting a normalization procedure for high-bias tunneling spectroscopy."

- date: 23 November 2022
  headline: "Robbie Elbertse successfully defended his [PhD thesis](https://doi.org/10.4233/uuid:e9374d12-699f-46e5-b963-ae061690b0b3) titled _Lifetime of atomic spin chains_. [<img src='/images/pubpic/PhD_Robbie.jpg' class='img-responsive' style='width: 100%; max-width: 160px' />](https://doi.org/10.4233/uuid:e9374d12-699f-46e5-b963-ae061690b0b3)"

- date: 28 October 2022
  headline: "Published in [Nano Letters](https://doi.org/10.1021/acs.nanolett.2c02783): _Experimental determination of a single atom ground state orbital through hyperfine anisotropy_ by Laëtitia Farinacci and coworkers. [<img src='/images/newspic/Nano_Letters.png' class='img-responsive' style='width: 100%; max-width: 160px' />](https://doi.org/10.1021/acs.nanolett.2c02783)"

- date: 27 September 2022
  headline: "Rasa Rejali successfully defended her [PhD thesis](https://doi.org/10.4233/uuid:02cd1165-9fef-4613-a12c-dc3c4bd9d731) _Building blocks for atomically assembled magnetic and electronic artificial lattices_. [<img src='/images/pubpic/PhD_Rasa.jpg' class='img-responsive' style='width: 100%; max-width: 160px' />](https://doi.org/10.4233/uuid:02cd1165-9fef-4613-a12c-dc3c4bd9d731)"

- date: 11 July 2022
  headline: "Our paper _Confined vacuum resonances as artificial atoms with tunable lifetime_, by Rasa Rejali and coworkers, was published in [ACS Nano](https://doi.org/10.1021/acsnano.2c04574). [<img src='/images/newspic/ACS_Nano.png' class='img-responsive' style='width: 100%; max-width: 250px' />](https://doi.org/10.1021/acsnano.2c04574)"

- date: 16 December 2021
  headline: "Postdoc Koen Bastiaans was awarded a [Veni grant](https://www.tudelft.nl/en/2021/tnw/nwo-veni-grant-for-4-researchers-of-tnw-faculty) from NWO. He will use the grant to study emergent quantum states atom-by-atom."

- date: 28 May 2021
  headline: "Today in [Science](https://doi.org/10.1126/science.abg8223), Lukas Veldman and coworkers demonstrate a new method to trace the coherent dynamics of two coupled atomic spins. The technique, which could be described as a means to overhear a conversation between the atoms, was covered in many news outlets including [Gizmodo](https://gizmodo.com/physicists-caught-two-atoms-talking-to-each-other-1846967872)."


