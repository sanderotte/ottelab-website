# OtteLab website

This is the website of our academic research group at Delft University of Technology. The website, powered by Jekyll, was forked from the Allan Lab website of Leiden University: https://allanlab.org/.

Copyright Sander Otte
