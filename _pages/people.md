---
title: "OtteLab | People"
layout: gridlay
excerpt: "OtteLab | People"
sitemap: false
permalink: /people/
---

## Group Members
<br />
<p>
<img src='/images/teampic/Kaag2024.jpg' class="img-responsive" style='width: 100%; max-width: 600px' />
</p>
Interested in joining us? Check our [Openings]({{ site.url }}{{ site.baseurl }}/openings).
<br />
<br />

### Current Staff

{% assign number_printed = 0 %}
{% for member in site.data.staff %}

{% assign even_odd = number_printed | modulo: 2 %}

{% if even_odd == 0 %}
<div class="row">
{% endif %}

<div class="col-sm-6 clearfix">
  <img src="{{ site.url }}{{ site.baseurl }}/images/teampic/{{ member.photo }}" class="img-responsive" width="25%" style="float: left; border-radius:5px" />
  <h4>{{ member.name }}</h4>
  <i>{{ member.info }} </i><br />
  <i> office: </i>{{ member.office }}<br />
  <i> email: </i><{{ member.email }}><br />
<!--
  <i> phone: </i>{{ member.phone }}<br />
  <i> present: </i>{{ member.present }}<br />
--> 

  
  <ul style="overflow: hidden">

<!--

  {% if member.number_educ == 1 %}
  <li> {{ member.education1 }} </li>
  {% endif %}

  {% if member.number_educ == 2 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  {% endif %}

  {% if member.number_educ == 3 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  <li> {{ member.education3 }} </li>
  {% endif %}

  {% if member.number_educ == 4 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  <li> {{ member.education3 }} </li>
  <li> {{ member.education4 }} </li>
  {% endif %}

  {% if member.number_educ == 5 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  <li> {{ member.education3 }} </li>
  <li> {{ member.education4 }} </li>
  <li> {{ member.education5 }} </li>
  {% endif %}

-->

  </ul>
</div>

{% assign number_printed = number_printed | plus: 1 %}

{% if even_odd == 1 %}
</div>
{% endif %}

{% endfor %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if even_odd == 1 %}
</div>
{% endif %}


<!-- ### Postdocs and PhD students -->
{% assign number_printed = 0 %}
{% for member in site.data.team_members %}

{% assign even_odd = number_printed | modulo: 2 %}

{% if even_odd == 0 %}
<div class="row">
{% endif %}

<div class="col-sm-6 clearfix">
  <img src="{{ site.url }}{{ site.baseurl }}/images/teampic/{{ member.photo }}" class="img-responsive" width="25%" style="float: left; border-radius:5px" />
  <h4>{{ member.name }}</h4>
  <i>{{ member.info }} </i><br />
  <i> office: </i>{{ member.office }}<br />
  <i> email: </i><{{ member.email }}><br />
 

  
  <ul style="overflow: hidden">

<!--

  {% if member.number_educ == 1 %}
  <li> {{ member.education1 }} </li>
  {% endif %}

  {% if member.number_educ == 2 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  {% endif %}

  {% if member.number_educ == 3 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  <li> {{ member.education3 }} </li>
  {% endif %}

  {% if member.number_educ == 4 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  <li> {{ member.education3 }} </li>
  <li> {{ member.education4 }} </li>
  {% endif %}

  {% if member.number_educ == 5 %}
  <li> {{ member.education1 }} </li>
  <li> {{ member.education2 }} </li>
  <li> {{ member.education3 }} </li>
  <li> {{ member.education4 }} </li>
  <li> {{ member.education5 }} </li>
  {% endif %}

-->

  </ul>
</div>

{% assign number_printed = number_printed | plus: 1 %}

{% if even_odd == 1 %}
</div>
{% endif %}

{% endfor %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if even_odd == 1 %}
</div>
{% endif %}


<!--

<br />
### Group Alumni

{% assign number_printed = 0 %}
{% for member in site.data.alumni_members %}

{% assign even_odd = number_printed | modulo: 2 %}

{% if even_odd == 0 %}
<div class="row">
{% endif %}

<div class="col-sm-6 clearfix">
  <img src="{{ site.url }}{{ site.baseurl }}/images/teampic/{{ member.photo }}" class="img-responsive" width="25%" style="float: left; border-radius:5px" />
  <h4>{{ member.name }}</h4>
  <i>{{ member.info }}</i><br /> {{ member.duration }}
  <ul style="overflow: hidden">

  </ul>
</div>

{% assign number_printed = number_printed | plus: 1 %}

{% if even_odd == 1 %}
</div>
{% endif %}

{% endfor %}

{% assign even_odd = number_printed | modulo: 2 %}
{% if even_odd == 1 %}
</div>
{% endif %}

-->

<br />
### Group alumni
<div class="row">

<div class="col-sm-4 clearfix">
<h4>Postdocs</h4>
{% for member in site.data.alumni_postdoc %}
{{ member.name }}
{% endfor %}
<br />
<h4>PhD students</h4>
{% for member in site.data.alumni_phd %}
{{ member.name }} ([PhD {{ member.year }}]({{ member.url }}))
{% endfor %}
<br />
</div>

<div class="col-sm-4 clearfix">
<h4>MSc students</h4>
{% for member in site.data.alumni_msc %}
{{ member.name }}
{% endfor %}
<br />
</div>

<div class="col-sm-4 clearfix">
<h4>BSc students</h4>
{% for member in site.data.alumni_bsc %}
{{ member.name }}
{% endfor %}
<br />
</div>

</div>
<br />


